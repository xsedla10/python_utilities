# Python Utilities

## About

This repository contains several Python utilities I created. These include
a safeclass, an alternative to dataclass, and support for tail call optimization.

## Typing

Programming decorators in Python is notoriously difficult to type. The end goal
is to make these decorators pass strict mypy checks. So far, mypy might scream.
