"""
This utility makes it easy to write recursion with tail call
optimization. To make a function utilize tail call optimization,
decorate a function with a "tail_call" decorator, and wrap the
return value with "Res", recursive descent with "Recursive".

TODO: support for tail call optimization when a different
function is called in return
"""

from __future__ import annotations
import sys


class Res:
    __slots__ = 'res'

    def __init__(self, res):
        self.res = res


class Recursive:
    __slots__ = 'args', 'kwargs'

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


class Fold:
    __slots__ = 'partial_res', 'next_call'

    def __init__(self, partial_res, next_call):
        self.partial_res = partial_res
        self.next_call = next_call


def tail_recursion(f):
    def _wrap(*args, **kwargs):
        res: Res | Recursive = Recursive(*args, **kwargs)

        while isinstance(res, Recursive):
            res = f(*res.args, **res.kwargs)

        return res.res

    return _wrap


def fold_tail_recursion(function, initial):
    def _wrap(f):

        def _wrap_wrap(*args, **kwargs):
            accumulator = initial
            res: Res | Recursive = Recursive(*args, **kwargs)

            while True:
                fold = f(*res.args, **res.kwargs)

                if isinstance(fold, Fold):
                    accumulator = function(accumulator, fold.partial_res)
                    res = fold.next_call
                else:
                    return function(accumulator, fold.res)

        return _wrap_wrap

    return _wrap


sys.set_int_max_str_digits(0)


@tail_recursion
def factorial(x: int, accum=1):
    if x == 0:
        return Res(accum)
    return Recursive(x-1, accum*x)


print(factorial(10))
print(factorial(100))
print(factorial(10000))


def add(x: int, y: int) -> int:
    return x + y


@fold_tail_recursion(add, 0)
def recursive_sum(x: int) -> int:
    assert x >= 0

    if x == 0:
        return Res(0)

    return Fold(x, Recursive(x - 1))


print(recursive_sum(3))
print(recursive_sum(10))
print(recursive_sum(100000))
